﻿using System;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory{

    [SerializeField]
    public GameObject[] enemyPrefab;

    public GameObject FactoryMethod(int spawnEnemy)
    {
        GameObject enemy = Instantiate(enemyPrefab[spawnEnemy]);
        return enemy;
    }
    
   public GameObject FactoryMethod(string tag)
    {
        throw new NotImplementedException();
    }

    void IFactory.FactoryMethod(int spawnEnemy)
    {
        throw new NotImplementedException();
    }
  
}