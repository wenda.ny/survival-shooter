﻿using UnityEngine;

public interface IFactory
{
    GameObject FactoryMethod(string tag);
    void FactoryMethod(int spawnEnemy);
}
