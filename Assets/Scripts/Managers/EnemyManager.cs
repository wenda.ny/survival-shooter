﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

    [SerializeField]
    MonoBehaviour factory;
    IFactory Factory { get { return factory as IFactory; } }


    void Start ()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn ()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        int spawnEnemy= Random.Range(0, 3);
        // Memduplikasi enemy
<<<<<<< HEAD
        //Factory.FactoryMethod(spawnEnemy);
=======
        Factory.FactoryMethod(spawnEnemy);
>>>>>>> 171d22899583ea3fde1ad1475fb61e819493b7ee
    }
}
